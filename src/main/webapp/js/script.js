//TODO: Навести тут порядок!
function viewMessage() {
    window.alert("js works")
}

function goBack() {
	history.back()
}

function checkPhone() {
	const phone = document.querySelector('input[name=phone]');
	if (phone.value.match(/^\+380\d{9}$/)) {
		phone.setCustomValidity('');
	} else {
		phone.setCustomValidity('Phone is not valid (must be +380XXXXXXXXX)');
	}
}

function matchPasswords() {
  const password = document.querySelector('input[name=password]');
  const confirm = document.querySelector('input[name=confirm]');
  if (confirm.value === password.value) {
    confirm.setCustomValidity('');
  } else {
    confirm.setCustomValidity('Passwords do not match');
  }
}

function sortTableAlphabetical() {
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("myTable");
  switching = true;
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[0];
      y = rows[i + 1].getElementsByTagName("TD")[0];
      // Check if the two rows should switch place:
      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
        // If so, mark as a switch and break the loop:
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

function sortTable(tableName, n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  //table = document.getElementById("myTable2");
  table = document.getElementById(tableName);
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          //var arrow = rows[0].getElementsByTagName("TH")[n];
          //arrow.textContent = arrow.textContent.substring(0, arrow.textContent.length - 1) + "↑";
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          //var arrow = rows[0].getElementsByTagName("TH")[n];
          //arrow.textContent = arrow.textContent.substring(0, arrow.textContent.length - 1) + "↓";
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
  for (i = 0; i < rows[0].cells.length; i++) {
	var arrow = rows[0].getElementsByTagName("TH")[i];
  	arrow.textContent = arrow.textContent.substring(0, arrow.textContent.length - 1);// + "↑";
  	if (i != n) arrow.textContent += "↕";
  	else {
  	  if (dir === "asc") arrow.textContent += "↑";
  	  else arrow.textContent += "↓";
  	}
}
  //var arrow = rows[0].getElementsByTagName("TH")[n];
  //arrow.textContent = arrow.textContent.substring(0, arrow.textContent.length - 1);// + "↑";
  //if (dir === "asc") arrow.textContent += "↑";
  //else arrow.textContent += "↓";
}

