<%@ attribute name="name" required="true" %> 
<%@ attribute name="caption" required="true" %> 
<label for="${name}"><b>${caption}</b></label>
<input type="text" placeholder="Enter ${caption}" name="${name}" maxlength="30" required>