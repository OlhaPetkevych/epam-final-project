<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="hrefCommand" required="true" %>
<%@ attribute name="formCommand" required="true" %>
<%@ attribute name="formParam1Name"%>
<%@ attribute name="formParam1Value"%> 
<%@ attribute name="formParam2Name"%>
<%@ attribute name="formParam2Value"%> 
<%@ attribute name="formParam3Name"%>
<%@ attribute name="formParam3Value"%> 
<%@ attribute name="page" required="true" %> 
<%@ attribute name="pageCount" required="true" %> 
<%@ attribute name="pageSize" required="true" %> 
<%@ attribute name="minPossiblePage" required="true" %> 
<%@ attribute name="maxPossiblePage" required="true" %> 
<div class="page-chooser">
	Page ${page} of ${pageCount} |
	<c:choose>
		<c:when test="${page - 1 > 0}">
			<a href="controller?command=${hrefCommand}&page=${page-1}&pageSize=${pageSize}">Previous</a>
		</c:when>
		<c:otherwise>
			Previous
		</c:otherwise>
	</c:choose>
	<c:forEach var="p" begin="${minPossiblePage}" end="${maxPossiblePage}">
		<c:choose>
			<c:when test="${page == p}">${p}</c:when>
			<c:otherwise>
				<a href="controller?command=${hrefCommand}&page=${p}&pageSize=${pageSize}">${p}</a>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	<c:choose>
		<c:when test="${page + 1 <= pageCount}">
			<a href="controller?command=${hrefCommand}&page=${page+1}&pageSize=${pageSize}">Next</a>
		</c:when>
		<c:otherwise>
			Next
		</c:otherwise>
	</c:choose>
	|
	<form action="controller" class="inline-form">
		<input name="command" type="hidden" value="${formCommand}" />
		<input name="${formParam1Name}" type="hidden" value="${formParam1Value}"/>
		<input name="${formParam2Name}" type="hidden" value="${formParam2Value}"/>
		<input name="${formParam3Name}" type="hidden" value="${formParam3Value}"/>
		<select name="page">
			<c:forEach begin="1" end="${pageCount}" var="p">
				<option value="${p}" ${p == param.page ? 'selected' : ''}>${p}</option>
			</c:forEach>
		</select>		
		<input name="pageSize" type="hidden" value="${pageSize}" />
		<input type="submit" value="Go"/>
	</form>
	<form action="controller" class="inline-form right-form">
		<input name="command" type="hidden" value="${formCommand}" />
		<input name="${formParam1Name}" type="hidden" value="${formParam1Value}"/>
		<input name="${formParam2Name}" type="hidden" value="${formParam2Value}"/>
		<input name="${formParam3Name}" type="hidden" value="${formParam3Value}"/>
		<input name="page" type="hidden" value="${page}" />
		<label for="pageSize">Page Size:</label>
		<input name="pageSize" type="number" min="1" max="100" step="1" value="${pageSize}">
		<input type="submit" value="Change"/>
	</form>
</div>
	