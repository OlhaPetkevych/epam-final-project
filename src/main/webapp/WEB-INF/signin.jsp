<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mylib1" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
	<c:set var="title" value="Sign In"/>
	<%@ include file="head.jspf" %>
<body>
	<form action="controller" class="signing-form" method="post">
		<div class="container">
			<input type="hidden" name="command" value="signin">
		    <!-- <label for="login"><b>Login</b></label>
		    <input type="text" placeholder="Enter Login" name="login" maxlength="30" required>-->
		    <mylib1:textfield name="login" caption="Login"></mylib1:textfield>
		    <label for="password"><b>Password</b></label>
		    <input type="password" placeholder="Enter Password" name="password" required>
		    <button class="signing-button" type="submit">Sign In</button>
		    <!--  <label>
		      	<input type="checkbox" checked="checked" name="remember"> Remember me
		    </label>-->
		</div>
		<div class="container">
			<ul class="signinform">
				<li class="signinform"><a class="backlink" href="controller">Go back</a></li>
				<!--<li><span class="psw">Forgot <a href="#">password?</a></span></li>-->
			</ul>
			<!-- <button type="button" class="cancelbtn" onclick="goBack()">Cancel</button>-->
		</div>
		<div class="container error">
			${signinErrorMessage}
			<c:remove var="signinErrorMessage" scope="session"/>
		</div>
	</form>
</body>
</html>