<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<c:set var="title" value="404 not found"/>
	<%@ include file="head.jspf" %>
<body>
<div class="centered-text"><h1>Error 404</h1><h2>Page not found</h2></div>
<ul class="start-page" >
	   <li><a class="active" href="${app}">Go to main page</a></li>
	</ul>
</body>
</html>