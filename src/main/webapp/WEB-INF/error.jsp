<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<c:set var="title" value="Error!"/>
	<%@ include file="head.jspf" %>
<body>
	<h2>Error!</h2>	
	<hr>	
	${requestScope['javax.servlet.error.exception'].message}
</body>
</html>