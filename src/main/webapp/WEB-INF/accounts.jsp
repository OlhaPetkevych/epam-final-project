<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mylib1" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="mylib2" uri="http://my.com"  %>
<!DOCTYPE html>
<html>
	<c:set var="title" value="Accounts"/>
	<%@ include file="head.jspf" %>
<body>
	<p class="right-aligned-text">You are signed as: ${currentUser.getLogin()}</p>
	<ul class="menu">
	    <li class="menu"><a href="controller?command=accountsPage&clientId=${clientId}&sortby=id&sorttype=asc&page=${page}&paeSize=${pageSize}" class="active">Accounts</a></li>
	    <li class="menu"><a href="controller?command=paymentsPage">Payments</a></li>
	</ul>
	<form action="controller" class="sorting-form">
		<strong>Sort by:</strong><br>
		<input name="command" type="hidden" value="accountsPage" />
		<input name="clientId" type="hidden" value="${clientId}"/>
		<input name="page" type="hidden" value="${page}"/>
		<input name="pageSize" type="hidden" value="${pageSize}"/>
		<label for="sortbyid">Id</label>
		<input type="radio" id="sortbyid" name="sortby" value="id" checked>
		<label for="sortbyaccount_name">Account name</label>
		<input type="radio" id="sortbyaccount_name" name="sortby" value="account_name">
		<label for="sortbybalance">Balance</label>
		<input type="radio" id="sortbybalance" name="sortby" value="balance">
		<br>
		<label for="sorttypeasc">Ascending</label>
		<input type="radio" id="sorttypeasc" name="sorttype" value="asc" checked>
		<label for="sorttypedesc">Descending</label>
		<input type="radio" id="sorttypedesc" name="sorttype" value="desc">
		<input type="submit" value="Sort"/>
	</form> 
	<mylib1:page-chooser 
	hrefCommand="accountsPage&clientId=${clientId}&sortby=${sortby}&sorttype=${sorttype}" 
	formCommand="accountsPage" formParam1Name="clientId" formParam1Value="${clientId}" 
	formParam2Name="sortby" formParam2Value="${sortby}" formParam3Name="sorttype" formParam3Value="${sorttype}"
	page="${page}" pageCount="${pageCount}" pageSize="${pageSize}" 
	minPossiblePage="${minPossiblePage}" maxPossiblePage="${maxPossiblePage}"/>
	<table>
		<tr>
			<th >Id</th>
			<th>Card Number</th>
			<th>Account Name</th>
			<th>Balance</th>
			<th><!-- Toping up account --></th>
			<th class="centered-text">Blocking account</th>
		</tr>
		<c:forEach items="${clientAccounts}" var="account">
			<tr>
				<td>${account.getId()}</td>
				<td>${account.getCardNumber()}</td>
				<td>${account.getAccountName()}</td>
				<td><mylib2:format-balance value="${account.getBalance()}"/></td>
				<td>
				<c:if test="${account.isBlocked() == false}">
					<form action="controller">
						<input name="command" type="hidden" value="topUp" />
						<button class="tablebtn greenbg" type="submit">Top up</button>
					</form>					
				</c:if>
				</td>
				<td>
					<c:choose>
						<c:when test="${account.isBlocked() == false}">
						<form action="controller" class="centered-text">
							<input name="command" type="hidden" value="blockAccount" />
							<button class="tablebtn redbg">Block account</button>
						</form>							
						</c:when>
						<c:when test="${account.isBlocked() == true && account.isUnlockRequest() == false}">
							<form action="controller" class="centered-text">
								<input name="command" type="hidden" value="unlockRequest" />
								<button class="tablebtn greenbg">Send unlock request</button>
							</form>	
						</c:when>
						<c:otherwise>
							<strong>Account is blocked and unlock request is already sent</strong>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</c:forEach>
	</table>
	<mylib1:page-chooser 
	hrefCommand="accountsPage&clientId=${clientId}&sortby=${sortby}&sorttype=${sorttype}" 
	formCommand="accountsPage" formParam1Name="clientId" formParam1Value="${clientId}"
	formParam2Name="sortby" formParam2Value="${sortby}" formParam3Name="sorttype" formParam3Value="${sorttype}" 
	page="${page}" pageCount="${pageCount}" pageSize="${pageSize}" 
	minPossiblePage="${minPossiblePage}" maxPossiblePage="${maxPossiblePage}"/>
</body>
</html>