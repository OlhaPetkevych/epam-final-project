<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mylib1" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<html>
	<c:set var="title" value="Sign up"/>
	<%@ include file="head.jspf" %>
<body>
	<form action="controller" class="signing-form" method="post">
		<div class="container">
			<h1 class="centered-text">Sign Up</h1>
			<p class="centered-text">Please fill in this form to create an account.</p>
			<input type="hidden" name="command" value="signup">
			<ul class="twocolumns">
				<li><mylib1:textfield name="lastName" caption="Last Name"></mylib1:textfield></li>
		    	<li><mylib1:textfield name="firstName" caption="First Name"></mylib1:textfield></li>
		    	<li><mylib1:textfield name="secondName" caption="Second Name"></mylib1:textfield></li>
		    	<li><mylib1:textfield name="login" caption="Login"></mylib1:textfield></li>
		    	<li><label for="phone"><b>Phone number (+380XXXXXXXXX)</b></label>
		    	<input type="tel" placeholder="+380XXXXXXXXX" name="phone" 
		    		onChange="checkPhone()" pattern=".{13}" required></li>
		    	<li><label for="email"><b>Email</b></label>
		    	<input type="email" placeholder="Enter email" name="email" maxlength="255" required></li>
			    <li><label for="password"><b>Password</b></label>
				<input type="password" placeholder="Enter Password" name="password" 
					onChange="matchPasswords()" required>
			    </li>
			    <li><label for="confirm"><b>Repeat Password</b></label>
		    	<input type="password" placeholder="Repeat Password" name="confirm" 
		    		onChange="matchPasswords()" required></li>
		    	<li><a class="cancellink" href="controller">Cancel</a></li>
		    	<li><button type="submit" class="signing-button signupbtn">Sign Up</button></li>
    		</ul>
		</div>
		<div class="container error">
			${signupErrorMessage}
			<c:remove var="signupErrorMessage" scope="session"/>
		</div>
	</form>
</body>
</html>