<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<c:set var="title" value="Start page"/>
	<%@ include file="head.jspf" %>
<body>
	<ul class="start-page" >
	  <li><h2>Payments system</h2></li>
	   <li><a class="active" href="controller?command=signinPage">Sign In</a></li>
	  <li class="text1">or</li>
	  <li><a class="active" href="controller?command=signupPage">Sign Up</a></li>
	</ul> 
</body>
</html>