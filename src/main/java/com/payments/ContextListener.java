package com.payments;


import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.annotation.*;

import org.apache.logging.log4j.*;

import com.payments.db.dao.DaoFactory;
import com.payments.db.datasource.DataSource;
import com.payments.db.datasource.DataSourceHikari;
import com.payments.service.AccountService;
import com.payments.service.UserService;
import com.payments.service.impl.AccountServiceImpl;
import com.payments.service.impl.UserServiceImpl;
import com.payments.command.*;

@WebListener
public class ContextListener implements ServletContextListener {

	public static Logger logger;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext ctx = sce.getServletContext();
		String path = ctx.getRealPath("/WEB-INF/log4j2.log");
		System.setProperty("logFile", path);
		//final Logger 
		logger = LogManager.getLogger(ContextListener.class);
		logger.debug("Starting...");
		logger.debug("path = " + path);
		logger.debug("Getting connection from DB...");
		DataSource dataSource = new DataSourceHikari(ctx.getRealPath("/WEB-INF/classes/datasource.properties"));
		try {
			dataSource.getConnection().close();
		} catch (SQLException ex) {
			logger.fatal("Cannot obtain a connection from DB", ex);
			throw new IllegalStateException("Cannot obtain a connection from DB", ex);
		}
		logger.debug("Connection from DB succesfully obtained");
		logger.debug("Initializing DAO Factory...");
		DaoFactory daoFactory = DaoFactory.getDaoFactory("MYSQL", dataSource);
		logger.debug("DAO Factory: " + daoFactory.getClass().getSimpleName());
		logger.debug("Creating services...");
		UserService userService = new UserServiceImpl(daoFactory.getUserDao(), daoFactory.getClientDao());
		AccountService accountService = new AccountServiceImpl(daoFactory.getAccountDao());
		logger.debug("Services created successfully");
		logger.debug("Initializing command container...");
		CommandContainer commands = new CommandContainer();
		Command command = new IndexPageCommand();
		commands.addCommand(null, command);
		commands.addCommand("", command);
		commands.addCommand("signinPage", new SigninPageCommand());
		commands.addCommand("signupPage", new SignupPageCommand());
		commands.addCommand("accountsPage", new AccountsPageCommand(accountService));
		commands.addCommand("paymentsPage", new PaymentsPageCommand());
		commands.addCommand("signin", new SigninCommand(userService));
		commands.addCommand("signup", new SignupCommand(userService));
		commands.addCommand("topUp", new TopUpCommand(accountService));
		commands.addCommand("blockAccount", new BlockAccountCommand(accountService));
		commands.addCommand("unlockRequest", new UnlockRequestCommand(accountService));
		logger.debug(commands);
	}

}




/*import java.security.NoSuchAlgorithmException;
import com.payments.util.Password;
 * try {
			Password.matchPasswords("test", "test", "test");
		} catch (NoSuchAlgorithmException ex) {
			logger.fatal("Cannot match passwords", ex);
			throw new IllegalStateException("Cannot match passwords", ex);
		}
		logger.debug("Matching passwords working");*/
