package com.payments.command;

import java.util.HashMap;
import java.util.Map;

public class CommandContainer {
	
	private static Map<String, Command> commands = new HashMap<>();
	
	/*static {
		commands = new HashMap<>();
		commands.put("signin", new SigninCommand());
		commands.put("signup", new SignupCommand());
	}*/
	
	public void addCommand(String name, Command command) {
		commands.put(name, command);
	}

	public static Command getCommand(String commandName) {
		return commands.get(commandName);
	}

	@Override
	public String toString() {
		return "CommandContainer=[" + commands + "]";
	}
}