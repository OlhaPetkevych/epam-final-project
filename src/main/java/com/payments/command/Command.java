package com.payments.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.payments.ContextListener;
//import com.payments.Controller;

public abstract class Command {
	//final static Logger logger = LogManager.getLogger(Command.class);
	final static Logger logger = ContextListener.logger;
	public abstract String execute(HttpServletRequest req, HttpServletResponse resp);
	@Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
