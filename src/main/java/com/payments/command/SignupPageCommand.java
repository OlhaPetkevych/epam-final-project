package com.payments.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SignupPageCommand extends Command {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		logger.debug("Going to signup page");
		return "/WEB-INF/signup.jsp";
	}

}
