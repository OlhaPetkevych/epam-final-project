package com.payments.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IndexPageCommand extends Command {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		logger.debug("Going to index page");
		return "/WEB-INF/index.jsp";
	}

}
