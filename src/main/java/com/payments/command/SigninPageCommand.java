package com.payments.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SigninPageCommand extends Command {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		logger.debug("Going to signin page");
		return "/WEB-INF/signin.jsp";
	}

}
