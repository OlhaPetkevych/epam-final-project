package com.payments.command;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.payments.db.entity.User;
import com.payments.db.exception.DBException;
import com.payments.db.exception.NotAllowedException;
import com.payments.db.exception.NotFoundException;
import com.payments.service.UserService;

public class SigninCommand extends Command {
	private final UserService userService;
	
	public SigninCommand(UserService userService) {
		this.userService = userService;
	}
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		logger.debug("Executing SigninCommand");
		logger.debug("Server side validation");
		String login = req.getParameter("login");
		logger.debug("login ==> " + login);
		String password = req.getParameter("password");
		String validationMessage = userService.validateSigninData(login, password);
		if (validationMessage.length() > 0) {
			req.getSession().setAttribute("signinErrorMessage", validationMessage);
			logger.debug("Data didn't pass validation: " + validationMessage);
			return "controller?command=signinPage";
		}
		logger.debug("Data is valid");
		try {
			logger.debug("Signing in...");
			User user = userService.signin(login, password);
			logger.debug("user ==> " + user);
			req.getSession().setAttribute("currentUser", user);
			//TODO: create welcome page
			return user.getRole()==1 ? "controller?command=usersPage" 
					: "controller?command=accountsPage&clientId=" + user.getId() 
					+ "&sortby=id&sorttype=asc&page=1&pageSize=5";
		} catch (NotFoundException | NotAllowedException e) {
			req.getSession().setAttribute("signinErrorMessage", e.getMessage());
			logger.debug("Message to user:" + e.getMessage());
			return "controller?command=signinPage";
		} catch (DBException e) {
			logger.error(e.getMessage());
			throw new RuntimeException(e);
		} catch (NoSuchAlgorithmException e) {
			String errText = "System dosen't work correctly!\nSerious problem with matching passwords!";
			logger.error(errText, e.getMessage());
			throw new RuntimeException("Sorry!\n" + errText);
		}
	}
}
