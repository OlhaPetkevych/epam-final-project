package com.payments.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PaymentsPageCommand extends Command {

	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		logger.debug("Going to payments page");
		return "/WEB-INF/payments.jsp";
	}

}
