package com.payments.command;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.payments.db.entity.Account;
import com.payments.db.exception.DBException;
import com.payments.service.AccountService;

public class AccountsPageCommand extends Command {

	private final AccountService accountService;
	
	public AccountsPageCommand(AccountService accountService) {
		this.accountService = accountService;
	}
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		logger.debug("Going to accounts page");
		String paramCilentId = req.getParameter("clientId");
		String sortby = req.getParameter("sortby");
		logger.debug("Sort by ==> " + sortby);
		String sorttype = req.getParameter("sorttype");
		logger.debug("Sort type ==> " + sorttype);
		String paramPage = req.getParameter("page");
		String paramPageSize = req.getParameter("pageSize");
		int clientId = Integer.parseInt(paramCilentId);
		logger.debug("clientId ==> " + clientId);
		
		int page = Integer.parseInt(paramPage);
		logger.debug("page ==> " + page);
		int pageSize = Integer.parseInt(paramPageSize);
		logger.debug("pageSize ==> " + pageSize);
		try {
			ArrayList<Account> clientAccounts 
				= accountService.getAllByClient(clientId, pageSize * (page - 1), pageSize, sortby + sorttype);
			int countByClient = accountService.getCountByClient(clientId);
			int minPagePossible = page - 1 < 1 ? 1 : page - 1;
			int pageCount = (int)Math.ceil((double)countByClient / (double)pageSize);
			int maxPagePossible = page + 1 > pageCount ? pageCount : page + 1;
			logger.debug("countByClient ==> " + countByClient);
			logger.debug("minPagePossible ==> " + minPagePossible);
			logger.debug("pageCount ==> " + pageCount);
			logger.debug("maxPagePossible ==> " + maxPagePossible);
			req.getSession().setAttribute("clientAccounts", clientAccounts);
			req.setAttribute("clientId", clientId);
			req.setAttribute("sortby", sortby);
			req.setAttribute("sorttype", sorttype);
			req.setAttribute("pageCount", pageCount);
			req.setAttribute("page", page);
			req.setAttribute("pageSize", pageSize);
			req.setAttribute("minPossiblePage", minPagePossible);
			req.setAttribute("maxPossiblePage", maxPagePossible);
			return "/WEB-INF/accounts.jsp";
		} catch (DBException e) {
			logger.error(e.getMessage());
			throw new RuntimeException(e);
		}		
	}

}
