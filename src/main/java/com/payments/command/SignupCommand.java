package com.payments.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.payments.db.entity.User;
import com.payments.db.exception.DBException;
import com.payments.service.UserService;

public class SignupCommand extends Command {
	private final UserService userService;
	
	public SignupCommand(UserService userService) {
		this.userService = userService;
	}
	
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		logger.debug("Executing SignupCommand");
		logger.debug("Server side validation");
		String lastName = req.getParameter("lastName");
		logger.debug("lastName ==> " + lastName);
		String firstName = req.getParameter("firstName");
		logger.debug("firstName ==> " + firstName);
		String secondName = req.getParameter("secondName");
		logger.debug("secondName ==> " + secondName);
		String login = req.getParameter("login");
		logger.debug("login ==> " + login);
		String phone = req.getParameter("phone");
		logger.debug("phone ==> " + phone);
		String email = req.getParameter("email");
		logger.debug("email ==> " + email);
		String password = req.getParameter("password");
		String confirm = req.getParameter("confirm");
		String validationMessage = userService.validateSignupData(lastName, firstName, secondName, login, phone, email, password, confirm);
		if (validationMessage.length() > 0) {
			req.getSession().setAttribute("signupErrorMessage", validationMessage);
			logger.debug("Data didn't pass validation: " + validationMessage);
			return "controller?command=signupPage";
		}
		logger.debug("Data is valid");
		try {
			String checkingInDatabaseMessage = userService.checkIfFreeInDatabase(login, phone, email);
			if (checkingInDatabaseMessage.length() > 0) {
				req.getSession().setAttribute("signupErrorMessage", checkingInDatabaseMessage);
				return "controller?command=signupPage";
			}
		} catch (DBException e) {
			logger.error(e.getMessage() + "Problem in DB!");
			throw new RuntimeException("Sorry! Problem in DB!");
		}
		try {
			User user = userService.signup(lastName, firstName, secondName, login, phone, email, password);
			req.getSession().setAttribute("currentUser", user);
			//TODO: create welcome page
			return "controller?command=accountsPage&clientId=" + user.getId() 
			+"&sortby=id&sorttype=asc&page=1&pageSize=5";
		} catch (DBException e) {
			logger.error(e.getMessage(), e);
			throw new RuntimeException("Sorry! Cannot create client!");
		}
	}
}
