package com.payments;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.payments.command.Command;
import com.payments.command.CommandContainer;

@WebServlet("/controller")
public class Controller extends HttpServlet {
	final static Logger logger = LogManager.getLogger(Controller.class);
		
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.debug("method ==> doGet");
		req.setCharacterEncoding("UTF-8");
		String address = "error.jsp";
		String commandName = req.getParameter("command");
		logger.debug("commandName ==> " + commandName);
		Command command = CommandContainer.getCommand(commandName);
		logger.debug("command ==> " + command);
		try {
			address = command.execute(req, resp);
		} catch (NullPointerException ex) {
			req.setAttribute("errorMessage", ex.getMessage());
			logger.error("no such command", ex);
		}
		logger.debug("going to page ==> " + address);
		req.getRequestDispatcher(address).forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		logger.debug("method ==> doPost");
		req.setCharacterEncoding("UTF-8");
		String address = "error.jsp";
		String commandName = req.getParameter("command");
		logger.debug("commandName ==> " + commandName);
		Command command = CommandContainer.getCommand(commandName);
		logger.debug("command ==> " + command);
		try {
			address = command.execute(req, resp);
		} catch (NullPointerException ex) {
			req.getSession().setAttribute("errorMessage", "Sorry! Serious problem!");
			logger.error("no such command", ex);
		}
		logger.debug("going to page ==> " + address);
		resp.sendRedirect(address);
	}
}
