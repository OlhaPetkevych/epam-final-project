package com.payments.db.entity;

import java.io.Serializable;

public class Client implements Serializable {
    private int User;
    private String LastName;
    private String FirstName;
    private String SecondName;
    private String Phone;
    private String Email;

    public Client() {
    }

    public Client(int user, String lastName, String firstName, String secondName, String phone, String email) {
        User = user;
        LastName = lastName;
        FirstName = firstName;
        SecondName = secondName;
        Phone = phone;
        Email = email;
    }

    public int getUser() {
        return User;
    }

    public void setUser(int user) {
        User = user;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    @Override
    public String toString() {
        return "Client{" +
                "User=" + User +
                ", LastName='" + LastName + '\'' +
                ", FirstName='" + FirstName + '\'' +
                ", SecondName='" + SecondName + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Email='" + Email + '\'' +
                '}';
    }
}
