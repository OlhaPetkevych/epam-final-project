package com.payments.db.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Payment implements Serializable {
	private Integer Id;
    private Long Amount;
    private LocalDateTime PaymentDate;
    private int SenderId;
    private int RecipientId;
    private int Status;

    public Payment() {
    }

    public Payment(Long amount, LocalDateTime paymentDate, int senderId, int recipientId, int status) {
        Amount = amount;
        PaymentDate = paymentDate;
        SenderId = senderId;
        RecipientId = recipientId;
        Status = status;
    }

    public Payment(Integer id, Long amount, LocalDateTime paymentDate, int senderId, int recipientId, int status) {
        Id = id;
        Amount = amount;
        PaymentDate = paymentDate;
        SenderId = senderId;
        RecipientId = recipientId;
        Status = status;
    }
    
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Long getAmount() {
        return Amount;
    }

    public void setAmount(Long amount) {
        Amount = amount;
    }

    public LocalDateTime getPaymentDate() {
        return PaymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        PaymentDate = paymentDate;
    }

    public int getRecipientId() {
        return RecipientId;
    }

    public void setRecipientId(int recipientId) {
    	RecipientId = recipientId;
    }
    
    public int getSenderId() {
        return SenderId;
    }

    public void setSenderId(int senderId) {
    	SenderId = senderId;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "Id=" + Id +
                ", Amount=" + Amount +
                ", PaymentDate=" + PaymentDate +
                ", SenderId=" + SenderId +
                ", RecipientId=" + RecipientId +
                ", Status=" + Status +
                '}';
    }
}
