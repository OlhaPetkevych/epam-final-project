package com.payments.db.entity;

import java.io.Serializable;

public class User implements Serializable {
	private Integer Id;
    private String Login;
    private transient String HashPassword;
    private transient String Salt;
    private int Role;
    private boolean blocked;

    public User() {
    }

    public User(Integer id, String login, String hashPassword, String salt, int role, boolean blocked) {
        Id = id;
        Login = login;
        HashPassword = hashPassword;
        Salt = salt;
        Role = role;
        this.blocked = blocked;
    }

    public User(String login, String hashPassword, String salt, int role, boolean blocked) {
        Login = login;
        HashPassword = hashPassword;
        Salt = salt;
        Role = role;
        this.blocked = blocked;
    }
    
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public String getHashPassword() {
        return HashPassword;
    }

    public void setHashPassword(String hashPassword) {
        HashPassword = hashPassword;
    }

    public String getSalt() {
        return Salt;
    }

    public void setSalt(String salt) {
        Salt = salt;
    }

    public int getRole() {
        return Role;
    }

    public void setRole(int role) {
        Role = role;
    }

    public boolean isBlocked() { return blocked; }

    public void setBlocked(boolean blocked) { this.blocked = blocked; }

    @Override
    public String toString() {
        return "User{" +
                "Id=" + Id +
                ", Login='" + Login + '\'' +
                ", Role=" + Role +
                ", blocked=" + blocked +
                '}';
    }
}
