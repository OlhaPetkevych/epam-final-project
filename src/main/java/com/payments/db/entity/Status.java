package com.payments.db.entity;

import java.io.Serializable;

public class Status implements Serializable {
    private Integer Id;
	private String Name;
	
	public Status() {
    }

    public Status(String name) {
        Name = name;
    }

    public Status(Integer id, String name) {
        Id = id;
        Name = name;
    }
    
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "Status{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                '}';
    }
}
