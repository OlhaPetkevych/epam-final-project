package com.payments.db.entity;

import java.io.Serializable;

public class Account implements Serializable {
	private Integer Id;
	private Long CardNumber;
    private String AccountName;
    private Long Balance;
    private int Client;
    private boolean Blocked;
    private boolean UnlockRequest;

    public Account() {
    }

    public Account(Long cardNumber, String accountName, Long balance, int client, boolean blocked, boolean unlockRequest) {
    	CardNumber = cardNumber;
    	AccountName = accountName;
        Balance = balance;
        Client = client;
        Blocked = blocked;
        UnlockRequest = unlockRequest;
    }

    public Account(Integer id, Long cardNumber, String accountName, Long balance, int client, boolean blocked, boolean unlockRequest) {
        Id = id;
        CardNumber = cardNumber;
        AccountName = accountName;
        Balance = balance;
        Client = client;
        Blocked = blocked;
        UnlockRequest = unlockRequest;
    }
    
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }
    
    public Long getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(Long cardNumber) {
    	CardNumber = cardNumber;
    }

    public String getAccountName() {
        return AccountName;
    }

    public void setAccountName(String accountName) {
        AccountName = accountName;
    }

    public Long getBalance() {
        return Balance;
    }

    public void setBalance(Long balance) {
        Balance = balance;
    }

    public int getClient() {
        return Client;
    }

    public void setClient(int client) {
        Client = client;
    }

    public boolean isBlocked() {
        return Blocked;
    }

    public void setBlocked(boolean blocked) {
        Blocked = blocked;
    }

    public boolean isUnlockRequest() {
        return UnlockRequest;
    }

    public void setUnlockRequest(boolean unlockRequest) {
        UnlockRequest = unlockRequest;
    }

    @Override
    public String toString() {
        return "Account{" +
                "Id=" + Id +
                ", CardNumber=" + CardNumber +
                ", AccountName='" + AccountName + '\'' +
                ", Balance=" + Balance +
                ", Client=" + Client +
                ", Blocked=" + Blocked +
                ", UnlockRequest=" + UnlockRequest +
                '}';
    }
}
