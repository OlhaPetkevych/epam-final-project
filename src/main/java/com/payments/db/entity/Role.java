package com.payments.db.entity;

import java.io.Serializable;

public class Role implements Serializable {
	private Integer Id;
	private String Name;

    public Role() {
    }

    public Role(Integer id, String name) {
        Id = id;
        Name = name;
    }

    public Role(String name) {
        Name = name;
    }
    
    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "Role{" + "Id=" + Id + ", Name='" + Name + '\'' + '}';
    }
}
