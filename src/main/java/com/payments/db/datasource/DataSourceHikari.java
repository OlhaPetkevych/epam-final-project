package com.payments.db.datasource;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class DataSourceHikari implements DataSource {

	//private static HikariConfig config = new HikariConfig("datasource.properties");
	private HikariDataSource ds;
	
	public DataSourceHikari(String configPath) {
		/*HikariConfig config = new HikariConfig();
        config.setDriverClassName("com.mysql.cj.jdbc.Driver");
        config.setJdbcUrl( "jdbc:mysql://localhost/paymentsdb" );
        config.setUsername( "paymentsuser" );
        config.setPassword( "K%#uGoh1EL5+" );
        config.addDataSourceProperty( "cachePrepStmts" , "true" );
        config.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        config.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );*/
		HikariConfig config = new HikariConfig(configPath);
        ds = new HikariDataSource(config);
	}

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }

}
