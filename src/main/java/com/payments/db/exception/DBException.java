package com.payments.db.exception;

public class DBException extends Exception {
	public DBException(String errorMessage) {
		super(errorMessage);
	}
}
