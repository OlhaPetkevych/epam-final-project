package com.payments.db.dao;


import java.util.ArrayList;

import com.payments.db.entity.Account;
import com.payments.db.exception.DBException;

public interface AccountDao {
	public ArrayList<Account> getAllByClient(int clientId, int limit, int offset, String sortingType) throws DBException;
	
	public int getCountByClient(int clientId) throws DBException;
	
}
