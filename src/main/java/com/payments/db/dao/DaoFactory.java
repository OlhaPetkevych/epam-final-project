package com.payments.db.dao;

import com.payments.db.dao.mysql.MySQLDaoFactory;
import com.payments.db.datasource.DataSource;

public abstract class DaoFactory {

	/*public enum Factories {
		MYSQL
	}*/
	
	public abstract AccountDao getAccountDao();
	public abstract ClientDao getClientDao();
	public abstract PaymentDao getPaymentDao();
	public abstract RoleDao getRoleDao();
	public abstract StatusDao getSratusDao();
	public abstract UserDao getUserDao();
	
	public static DaoFactory getDaoFactory(/*Factories*/String factoryName, DataSource dataSource) {
		switch (factoryName) {
		case "MYSQL": return new MySQLDaoFactory(dataSource);
		default: return null;
		}
	}
}
