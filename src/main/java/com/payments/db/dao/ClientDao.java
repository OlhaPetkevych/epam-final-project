package com.payments.db.dao;

import com.payments.db.entity.Client;
import com.payments.db.exception.DBException;

public interface ClientDao {
	public Client getByUserId(int userId) throws DBException;

	public boolean phoneAlreadyExists(String phone) throws DBException;
	
	public boolean emailAlreadyExists(String email) throws DBException;
	
	public void insertClientTransaction(String lastName, String firstName, String secondName, String login, String phone, String email, String password) throws DBException;
}
