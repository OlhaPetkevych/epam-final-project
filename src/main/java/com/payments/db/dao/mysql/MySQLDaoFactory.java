package com.payments.db.dao.mysql;

import com.payments.db.dao.AccountDao;
import com.payments.db.dao.ClientDao;
import com.payments.db.dao.DaoFactory;
import com.payments.db.dao.PaymentDao;
import com.payments.db.dao.RoleDao;
import com.payments.db.dao.StatusDao;
import com.payments.db.dao.UserDao;
import com.payments.db.datasource.DataSource;

public class MySQLDaoFactory extends DaoFactory {

	private DataSource dataSource;
	
	public MySQLDaoFactory(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public AccountDao getAccountDao() {
		return new AccountDaoMySQL(dataSource);
	}

	@Override
	public ClientDao getClientDao() {
		return new ClientDaoMySQL(dataSource);
	}

	@Override
	public PaymentDao getPaymentDao() {
		return new PaymentDaoMySQL(dataSource);
	}

	@Override
	public RoleDao getRoleDao() {
		return new RoleDaoMySQL(dataSource);
	}

	@Override
	public StatusDao getSratusDao() {
		return new StatusDaoMySQL(dataSource);
	}

	@Override
	public UserDao getUserDao() {
		return new UserDaoMySQL(dataSource);
	}

}
