package com.payments.db.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.payments.ContextListener;
import com.payments.db.dao.AccountDao;
import com.payments.db.datasource.DataSource;
import com.payments.db.entity.Account;
import com.payments.db.exception.DBException;

public class AccountDaoMySQL implements AccountDao{
	//final static Logger logger = LogManager.getLogger(AccountDao.class);
	final static Logger logger = ContextListener.logger;
	
	private static final String GET_COUNT_BY_CLIENT = "SELECT COUNT(*) FROM Accounts WHERE client_id = ?";
	
	private Map<String, String> getAccountsStrings;
	private DataSource dataSource;
	
	public AccountDaoMySQL(DataSource dataSource) {
		getAccountsStrings = new HashMap<>();
		getAccountsStrings.put("idasc", 
				"SELECT id, card_number, account_name, balance, blocked, unlock_request FROM Accounts WHERE client_id = ? limit ? offset ?");
		getAccountsStrings.put("iddesc", 
				"SELECT id, card_number, account_name, balance, blocked, unlock_request FROM Accounts WHERE client_id = ? ORDER BY id DESC limit ? offset ?");
		getAccountsStrings.put("account_nameasc", 
				"SELECT id, card_number, account_name, balance, blocked, unlock_request FROM Accounts WHERE client_id = ? ORDER BY account_name, id limit ? offset ?");
		getAccountsStrings.put("account_namedesc", 
				"SELECT id, card_number, account_name, balance, blocked, unlock_request FROM Accounts WHERE client_id = ? ORDER BY account_name DESC, id limit ? offset ?");
		getAccountsStrings.put("balanceasc", 
				"SELECT id, card_number, account_name, balance, blocked, unlock_request FROM Accounts WHERE client_id = ? ORDER BY balance, id limit ? offset ?");
		getAccountsStrings.put("balancedesc", 
				"SELECT id, card_number, account_name, balance, blocked, unlock_request FROM Accounts WHERE client_id = ? ORDER BY balance, id DESC limit ? offset ?");
		this.dataSource = dataSource;
	}
	
	@Override
	public ArrayList<Account> getAllByClient(int clientId, int limit, int offset, String sortingType) throws DBException {
		logger.debug("Start searching Accounts by clientId");
		logger.debug("sortingType ==> " + sortingType);
		String sql = getAccountsStrings.get(sortingType);
		if (sql == null) {
			logger.error("Problem with searching accounts by clientId - wrong sortingType");
			throw new DBException("Problem with searching accounts");
		}
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, clientId);
			preparedStatement.setInt(2, limit);
			preparedStatement.setInt(3, offset);
			ResultSet rs = preparedStatement.executeQuery();
			ArrayList<Account> accounts = new ArrayList<>();
			while (rs.next()) {
				accounts.add(new Account(rs.getInt(1),
						rs.getLong(2),
						rs.getString(3),
						rs.getLong(4),
						clientId,
						rs.getBoolean(5),
						rs.getBoolean(6)));
			}
			return accounts;
		} catch (SQLException ex) {
			logger.error("Problem with searching accounts by clientId", ex);
			throw new DBException("Problem with searching accounts");
		}
	}

	@Override
	public int getCountByClient(int clientId) throws DBException {
		logger.debug("Start counting Accounts by clientId");
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_COUNT_BY_CLIENT)) {
			preparedStatement.setInt(1, clientId);
			ResultSet rs = preparedStatement.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);				
			}
			return count;
		} catch (SQLException ex) {
			logger.error("Problem with counting accounts by clientId", ex);
			throw new DBException("Problem with counting accounts");
		}
	}
}
