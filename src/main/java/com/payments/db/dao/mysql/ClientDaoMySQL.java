package com.payments.db.dao.mysql;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.Logger;

import com.payments.ContextListener;
import com.payments.db.dao.ClientDao;
import com.payments.db.datasource.DataSource;
import com.payments.db.entity.Client;
import com.payments.db.exception.DBException;
import com.payments.db.exception.NotFoundException;
import com.payments.util.Password;

public class ClientDaoMySQL implements ClientDao {
	//final static Logger logger = LogManager.getLogger(ClientDao.class);
	final static Logger logger = ContextListener.logger;
	
	private static final String COUNT_PHONES = "SELECT COUNT(*) FROM clients WHERE phone = ?";
	private static final String COUNT_EMAILS = "SELECT COUNT(*) FROM clients WHERE email = ?";
	private static final String GET_CLIENT_BY_USER_ID = "SELECT * FROM clients WHERE user_id = ?";
	
	private static final String GET_USER_ID_BY_LOGIN = "SELECT id FROM users WHERE login = ?";
	private static final String INSERT_USER =
			"INSERT INTO users (login, hashpassword, salt, role_id) VALUES (?, ?, ?, ?)";
	private static final String INSERT_CLIENT =
			"INSERT INTO clients (user_id, last_name, first_name, second_name, phone, email) VALUES (?, ?, ?, ?, ?, ?)";
	
	private DataSource dataSource;
	
	public ClientDaoMySQL(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public Client getByUserId(int userId) throws DBException, NotFoundException {
		logger.debug("Start searching Client by userId");
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_CLIENT_BY_USER_ID)) {
				preparedStatement.setInt(1, userId);
				ResultSet rs = preparedStatement.executeQuery();
				while (rs.next()) {
					Client client = new Client(rs.getInt(1), 
							rs.getString(2),
							rs.getString(3),
							rs.getString(4),
							rs.getString(5),
							rs.getString(6));
					logger.debug("Client found: " + client);
					return client;
				}
		} catch (SQLException ex) {
			logger.error("Problem with getting client by user id", ex);
			throw new DBException("Problem with getting client from DB");
		}
		logger.debug("Client not found");
		throw new NotFoundException("Client");
	}
	
	@Override
	public boolean phoneAlreadyExists(String phone) throws DBException {
		logger.debug("Start checking if phone already exists");
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(COUNT_PHONES)) {
			preparedStatement.setString(1, phone);
			ResultSet rs = preparedStatement.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);
			}
			if (count == 1) {
				logger.debug("Phone already exists!");
				return true;
			} else {
				logger.debug("Phone is not in DB");
				return false;
			}
		} catch (SQLException ex) {
			logger.error("Problem with checking if phone already exists", ex);
			throw new DBException("Problem with checking if phone already exists");
		}
	}
	
	@Override
	public boolean emailAlreadyExists(String email) throws DBException {
		logger.debug("Start checking if email already exists");
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(COUNT_EMAILS)) {
			preparedStatement.setString(1, email);
			ResultSet rs = preparedStatement.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);
			}
			if (count == 1) {
				logger.debug("Email already exists!");
				return true;
			} else {
				logger.debug("Email is not in DB");
				return false;
			}
		} catch (SQLException ex) {
			logger.error("Problem with checking if email already exists", ex);
			throw new DBException("Problem with checking if email already exists");
		}
	}
	
	public void insertClientTransaction(String lastName, String firstName, String secondName, String login, String phone, String email, String password) throws DBException {
		Connection connection = null;
		PreparedStatement psInsertUser = null;
		PreparedStatement psGetUserId = null;
		PreparedStatement psInsertClient = null;
		try /*(Connection connection = dataSource.getConnection();
				PreparedStatement psInsertUser = connection.prepareStatement(INSERT_USER);
				PreparedStatement psGetUserId = connection.prepareStatement(GET_USER_ID_BY_LOGIN);
				PreparedStatement psInsertClient = connection.prepareStatement(INSERT_CLIENT))*/ {
			connection = dataSource.getConnection();
			psInsertUser = connection.prepareStatement(INSERT_USER);
			psGetUserId = connection.prepareStatement(GET_USER_ID_BY_LOGIN);
			psInsertClient = connection.prepareStatement(INSERT_CLIENT);
			connection.setAutoCommit(false);
			
			String salt = Password.getRandomString();
			String hash = Password.getSHA3256Hash(password, salt);
			psInsertUser.setString(1, login);
			psInsertUser.setString(2, hash);
			psInsertUser.setString(3, salt);
			psInsertUser.setInt(4, 2);
			psInsertUser.executeUpdate();
			
			psGetUserId.setString(1, login);
			ResultSet rs = psGetUserId.executeQuery();
			Integer userId = null;
			while (rs.next()) {
				userId = rs.getInt(1);
				break;
			}
			
			psInsertClient.setInt(1, userId);
			psInsertClient.setString(2, lastName);
			psInsertClient.setString(3, firstName);
			psInsertClient.setString(4, secondName);
			psInsertClient.setString(5, phone);
			psInsertClient.setString(6, email);
			psInsertClient.executeUpdate();
			
			connection.commit();
			connection.setAutoCommit(true);
			
		} catch (NoSuchAlgorithmException | SQLException ex) {
			try {
				if(connection != null) connection.rollback();
			} catch (SQLException e) {
				logger.error("Problem with inserting client - cannot rollback transaction!", e);
				throw new DBException("Problem with inserting client");
			}
			logger.error("Problem with inserting client", ex);
			throw new DBException("Problem with inserting client");
		} finally {
			try {
				if(connection != null) connection.close();
				if(psInsertUser != null) psInsertUser.close();
				if(psGetUserId != null) psGetUserId.close();
				if(psInsertClient != null) psInsertClient.close();
			} catch (SQLException e) {
				logger.error("Problem with inserting client - cannot close connection or statement!", e);
				throw new DBException("Problem with inserting client");
			}
		}
	}
}
