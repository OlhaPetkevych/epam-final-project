package com.payments.db.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.Logger;

import com.payments.ContextListener;
import com.payments.db.dao.UserDao;
import com.payments.db.datasource.DataSource;
import com.payments.db.entity.User;
import com.payments.db.exception.DBException;
import com.payments.db.exception.NotFoundException;

public class UserDaoMySQL implements UserDao {
	//final static Logger logger = LogManager.getLogger(UserDao.class);
	final static Logger logger = ContextListener.logger;
	
	private static final String GET_USER_BY_LOGIN = "SELECT * FROM Users WHERE login = ?";
	/*private static final String INSERT_USER =
			"INSERT INTO users (login, hashpassword, salt, role_id) VALUES (?, ?, ?, ?)";*/
	
	private DataSource dataSource;
	
	public UserDaoMySQL(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	/*@Override
	public void insert(User user) throws DBException {
		logger.debug("Start inserting User");
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER)) {
			preparedStatement.setString(1, user.getLogin());
			preparedStatement.setString(2, user.getHashPassword());
			preparedStatement.setString(3, user.getSalt());
			preparedStatement.setInt(4, user.getRole());
			long rows = preparedStatement.executeUpdate();
			if (rows == 1) {
				logger.debug("User inserted successfully");
			} else {
				logger.error("Problem with inserting user: inserted " + rows + "row(s)");
				throw new DBException("Problem with inserting user");
			}			
		} catch (SQLException ex) {
			logger.error("Problem with inserting user", ex);
			throw new DBException("Problem with inserting user");
		}
	}*/
	
	@Override
	public User getByLogin(String login) throws DBException, NotFoundException {
		logger.debug("Start searching User by login");
		try (Connection connection = dataSource.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(GET_USER_BY_LOGIN)) {
			preparedStatement.setString(1, login);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				User user = new User(rs.getInt(1), 
						rs.getString(2), 
						rs.getString(3), 
						rs.getString(4), 
						rs.getInt(5),
						rs.getBoolean(6));
				logger.debug("User found: " + user);
				return user;
			}
		} catch (SQLException ex) {
			logger.error("Problem with searching user", ex);
			throw new DBException("Problem with searching user");
		}
		logger.debug("User not found");
		throw new NotFoundException("Incorrect login!");
	}

}
