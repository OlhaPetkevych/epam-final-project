package com.payments.db.dao;

import com.payments.db.entity.User;
import com.payments.db.exception.DBException;

public interface UserDao {
	//public void insert(User user) throws DBException;
	
	public User getByLogin(String login) throws DBException;

}
