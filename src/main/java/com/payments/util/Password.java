package com.payments.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class Password {
	private final static String PEPPER = "W*Vfg^LN`lWv+TM0NW:Y;GM0GaVvK8d&/oS3:3*0>kK01UScv&YA*UlvZTL2i,kL";
	
	public static String getRandomString() {
        return new Random().ints(35, 123).limit(64)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }
	
	public static boolean matchPasswordWithHash(String input, String hash, String salt) throws NoSuchAlgorithmException {
        if ((input == null) || (hash == null) || (salt == null)) return false;
        return getSHA3256Hash(input, salt).equals(hash);
    }

    public static boolean matchPasswords(String input, String stored, String salt) throws NoSuchAlgorithmException {
        if ((input == null) || (stored == null) || (salt == null)) return false;
        return getSHA3256Hash(input, salt).equals(getSHA3256Hash(stored, salt));
    }
    
    //private 
    public static String getSHA3256Hash(String originalString, String salt) throws NoSuchAlgorithmException {
        //originalString += PEPPER;
        String result = originalString + salt + PEPPER;
        final MessageDigest digest = MessageDigest.getInstance("SHA3-256");
        final byte[] hashBytes = digest.digest(result.getBytes(StandardCharsets.UTF_8));
        return bytesToHex(hashBytes);
    }

    private static String bytesToHex(byte[] hashBytes) {
        StringBuilder result = new StringBuilder();
        for (byte aByte : hashBytes) {
            int decimal = (int) aByte & 0xff;
            String hex = Integer.toHexString(decimal);
            if (hex.length() % 2 == 1) {
                hex = "0" + hex;
            }
            result.append(hex);
        }
        return result.toString();
    }
}
