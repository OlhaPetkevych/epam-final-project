package com.payments.service.impl;

import java.security.NoSuchAlgorithmException;
import java.util.StringJoiner;

import org.apache.logging.log4j.Logger;

import com.payments.ContextListener;
import com.payments.db.dao.ClientDao;
import com.payments.db.dao.UserDao;
//import com.payments.db.entity.Client;
import com.payments.db.entity.User;
import com.payments.db.exception.DBException;
import com.payments.db.exception.NotAllowedException;
import com.payments.db.exception.NotFoundException;
import com.payments.service.UserService;
import com.payments.util.Password;

public class UserServiceImpl implements UserService {
	//final static Logger logger = LogManager.getLogger(UserService.class);
	final static Logger logger = ContextListener.logger;
	private final UserDao userDao;
	private final ClientDao clientDao;
	private final static String TEXT_VALIDATION_MESSAGE = " is empty or longer than 30 symbols!";
	
	public UserServiceImpl(UserDao userDao, ClientDao clientDao) {
		this.userDao = userDao;
		this.clientDao = clientDao;		
	}

	@Override
	public String checkIfFreeInDatabase(String login, String phone, String email) throws DBException {
		StringJoiner stringJoier = new StringJoiner(" ");
		try {
			User user = userDao.getByLogin(login);
			stringJoier.add("Login '" + login + "' is already in use!");
			logger.debug("User already exists: " + user);
		} catch (NotFoundException e) {
			logger.debug("Login is free");
		}
		if(clientDao.phoneAlreadyExists(phone)) {
			stringJoier.add("Phone '" + phone + "' is already in use!");
			logger.debug("Phone '" + phone + "' is already in use");
		}
		if(clientDao.emailAlreadyExists(email)) {
			stringJoier.add("Email '" + email + "' is already in use!");
			logger.debug("Email '" + email + "' is already in use");
		}
		return stringJoier.toString();
	}
	
	@Override
	public User signin(String login, String password) throws DBException, NoSuchAlgorithmException {
		logger.debug("method ==> signin");
		User user = userDao.getByLogin(login);
		logger.debug("user ==> " + user);
		if (user.isBlocked()) {
			logger.debug("User is blocked!");
			throw new NotAllowedException("Sorry, " + login + "! You are blocked");
		}
		if(Password.matchPasswordWithHash(password, user.getHashPassword(), user.getSalt())) {
			return user;
		} else {
			logger.debug("Incorrect password!");
			throw new NotAllowedException("Incorrect password!");
		}
	}

	@Override
	public User signup(String lastName, String firstName, String secondName, String login, String phone, 
			String email, String password) throws DBException {
		logger.debug("method ==> signup");
		clientDao.insertClientTransaction(lastName, firstName, secondName, login, phone, email, password);
		User user = userDao.getByLogin(login);
		logger.debug("user ==> " + user);
		/*Client client = clientDao.getByUserId(user.getId()); 
		logger.debug("client ==> " + client);*/
		return user;
	}

	@Override
	public String validateSigninData(String login, String password) {
		StringJoiner stringJoier = new StringJoiner(" ");
		if (login == null || login.length() > 30) {
			stringJoier.add("Login" + TEXT_VALIDATION_MESSAGE);
		}
		if (password == null) { 
			stringJoier.add("Password is empty!");
		}
		return stringJoier.toString();
	}

	@Override
	public String validateSignupData(String lastName, String firstName, String secondName, String login, 
			String phone, String email, String password, String confirm) {
		StringJoiner stringJoier = new StringJoiner(" ");
		if (lastName == null || lastName.length() > 30) {
			stringJoier.add("Last Name" + TEXT_VALIDATION_MESSAGE);
		}
		if (firstName == null || firstName.length() > 30) {
			stringJoier.add("First Name" + TEXT_VALIDATION_MESSAGE);
		}
		if (secondName == null || secondName.length() > 30) {
			stringJoier.add("Second Name" + TEXT_VALIDATION_MESSAGE);
		}
		if (login == null || login.length() > 30) {
			stringJoier.add("Login" + TEXT_VALIDATION_MESSAGE);
		}
		if (phone == null || phone.length() != 13) {
			stringJoier.add("Phone must have 13 symbols!");
		}
		if (email == null || email.length() > 255) {
			stringJoier.add("Email is empty or longer than 255 symbols!");
		}
		if (password == null || confirm == null || !password.equals(confirm)) {
			stringJoier.add("Passwords do not match!");
		}
		return stringJoier.toString();
	}

}
