package com.payments.service.impl;

import java.util.ArrayList;

import org.apache.logging.log4j.Logger;

import com.payments.ContextListener;
import com.payments.db.dao.AccountDao;
import com.payments.db.entity.Account;
import com.payments.db.exception.DBException;
import com.payments.service.AccountService;

public class AccountServiceImpl implements AccountService {
	//final static Logger logger = LogManager.getLogger(AccountService.class);
	final static Logger logger = ContextListener.logger;
	private final AccountDao accountDao;
	
	public AccountServiceImpl (AccountDao accountDao) {
		this.accountDao = accountDao;
	}
	
	@Override
	public ArrayList<Account> getAllByClient(int clientId, int limit, int offset, String sortingType) throws DBException{
		
		return accountDao.getAllByClient(clientId, offset, limit, sortingType);
	}

	@Override
	public int getCountByClient(int clientId) throws DBException {
		
		return accountDao.getCountByClient(clientId);
	}

}
