package com.payments.service;

import java.util.ArrayList;

import com.payments.db.entity.Account;
import com.payments.db.exception.DBException;

public interface AccountService {
	ArrayList<Account> getAllByClient(int clientId, int limit, int offset, String sortingType) throws DBException;
	int getCountByClient(int clientId) throws DBException;
}
