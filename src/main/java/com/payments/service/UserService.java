package com.payments.service;

import java.security.NoSuchAlgorithmException;

import com.payments.db.entity.User;
import com.payments.db.exception.DBException;

public interface UserService {
	String checkIfFreeInDatabase(String login, String phone, String email) throws DBException;
	User signin(String login, String password) throws DBException, NoSuchAlgorithmException;
	User signup(String lastName, String firstName, String secondName, String login, String phone, String email, String password) throws DBException;
	String validateSigninData(String login, String password);
	String validateSignupData(String lastName, String firstName, String secondName, String login, String phone, String email, String password, String confirm);
}
