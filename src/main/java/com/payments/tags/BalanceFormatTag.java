package com.payments.tags;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class BalanceFormatTag extends SimpleTagSupport {
	private long value;

	
	public void setValue(long value) {
		this.value = value;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		//TODO: add locale to constructor
		NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
		//Ukrainian: new Locale("uk_UA")
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		String result = numberFormat.format(value / 100.0);
		getJspContext().getOut().println(result);
	}
}
